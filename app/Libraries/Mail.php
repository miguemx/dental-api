<?php
namespace App\Libraries;

class Mail {

    public $from = null;
    public $fromName = null;
    public $to = null;
    public $subject = null;
    public $message = null;

    public $cc;
    public $bcc;

    protected $Email;
    protected $config;

    protected $templates = array(
        'register' => 'Register.html'
    );

    public function __construct() {
        $this->config = array(
            'protocol' => env('mail.protocol'),
            'SMTPHost' => env('mail.SMTPHost'),
            'SMTPUser' => env('mail.SMTPUser'),
            'SMTPPass' => env('mail.SMTPPass'),
            'SMTPPort' => env('mail.SMTPPort'),
            'SMTPCrypto' => env('mail.SMTPCrypto'),

            'mailType' => 'html',

            'CRLF' => "\r\n",
            'newline' => "\r\n"
        );
        $this->Email = \Config\Services::email();
        $this->Email->initialize( $this->config );
    }

    /**
     * envia un correo electronico a la direccion setteada en el objeto, y desde la direccion indicada en el objeto
     * se debe tener los valores de correo seteados, o tomara los de default
     * @return true en caso de exito
     * @return false en caso de que el correo no se pueda enviar, o no tenga destinatario
     */
    public function send() {
        $from = ( !is_null($this->from) )? $this->from: 'apps@privatecarservise.com';
        $fromName = ( !is_null($this->fromName) )? $this->fromName: 'Apps Private Car Service';
        if ( !is_null($this->to) ) {
            $this->Email->setFrom($from, $fromName);
            $this->Email->setTo( $this->to );
            if ( !is_null( $this->cc ) ) {
                $this->Email->setCC($this->cc);
            }
            if ( !is_null( $this->bcc ) ) {
                $this->Email->setBCC($this->bcc);
            }

            $subject = ( !is_null($this->subject) )? $this->subject: "Automated message from Apps Private Car Service";
            $this->Email->setSubject( $subject );

            $message = ( !is_null($this->message) ) ? $this->message: 'This is an automated message sent to your addres';
            $this->Email->setMessage( $message );

            $return = $this->Email->send();
            var_dump( $this->Email->printDebugger(['headers', 'subject', 'body']) );
            return $return;
        } 
        return false;
    }

    /**
     * obtiene el codigo HTML de la template para la creacion de un correo electronico
     * @param tipo string con el tipo de plantilla a obtener. debe existir en el arreglo de plantillas
     * @return output el codigo html obtenido
     * @return false si la plantilla no esta registrada
     */
    public function getTemplate($tipo) {
        if ( array_key_exists( $tipo, $this->templates) ) {
            $url = 'http://privatecarservise.com/templates/'.$this->templates[$tipo];
            $ch = curl_init(); // create curl resource
            curl_setopt($ch, CURLOPT_URL, $url); // set url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return the transfer as a string
            $output = curl_exec($ch); // $output contains the output string
            curl_close($ch);  // close curl resource to free up system resources

            return $output;
        }
        return false;
    }

    /**
     * envia un correo a un nuevo usuario con las instrucciones para validar su cuenta
     * @param email el correo para enviar el mensaje
     * @param name el nombre de la persona
     * @param token el token de validacion registrado
     */
    public function registration( $email, $name, $token ) {
        // {{var:user_name}}
        // {{var:confirmation_link}}
        $link = 'http://api.privatecarservise.com/Users/Verify/'.$token;
        $mensaje = $this->getTemplate( 'register' );
        $mensaje = str_replace( '{{var:user_name}}', $name, $mensaje );
        $mensaje = str_replace( '{{var:confirmation_link}}', $link, $mensaje );

        $this->to = $email;
        $this->subject = 'Private Car Service - Activation Email.';
        $this->message = $mensaje;

        return $this->send();
        
    }

}