<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Presupuesto extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'            => null,
        'paciente'      => null,
        'nombre'        => null,
        'descripcion'   => null,
        'status'        => null,
        'subtotal'       => null,
        'impuesto'      => null,
        'total'         => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'            => 'presupuesto_id',
        'paciente'      => 'presupuesto_paciente',
        'nombre'        => 'presupuesto_nombre',
        'descripcion'   => 'presupuesto_descripcion',
        'status'        => 'presupuesto_status',
        'subtotal'      => 'presupuesto_subtotal',
        'impuesto'      => 'presupuesto_impuesto',
        'total'         => 'presupuesto_total',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

}

