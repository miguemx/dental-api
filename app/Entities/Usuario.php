<?php

namespace App\Entities;

use CodeIgniter\Entity;

class Usuario extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'            => null,
        'rol'           => null,
        'persona'       => null,
        'username'      => null,
        'password'      => null,
        
        'created_at'    => null,
        'updated_at'    => null,
        'deleted_at'    => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'            => 'usuario_id',
        'rol'           => 'usuario_rol',
        'persona'       => 'usuario_persona',
        'username'      => 'usuario_nombre',
        'password'      => 'usuario_contrasena',
        
        'created_at'    => 'created_at',
        'updated_at'    => 'updated_at',
        'deleted_at'    => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}