<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Persona extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'medicoCabecera' => null,
        'nombre' => null,
        'apellido' => null,
        'correo' => null,
        'celular' => null,
        'domicilio' => null,
        'telefono' => null,
        'documentoId' => null,
        'curp' => null,
        'edad' => null,
        'fechaNacimiento' => null,
        'ciudad' => null,
        'estado' => null,
        'pais' => null,
        'sexo' => null,
        'foto' => null,
        'grupoSanguineo' => null,
        'alertas' => null,
        'esPaciente' => null,
        'esMedico' => null,
        'expediente' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                => 'persona_id',
        'medicoCabecera'    => 'persona_medico_cabecera',
        'nombre'            => 'persona_nombre',
        'apellido'          => 'persona_apellido',
        'correo'            => 'persona_correo',
        'celular'           => 'persona_celular',
        'domicilio'         => 'persona_domicilio',
        'telefono'          => 'persona_telefono',
        'documentoId'       => 'persona_documento_id',
        'curp'              => 'persona_curp',
        'edad'              => 'persona_edad',
        'fechaNacimiento'   => 'persona_fecha_nacimiento',
        'ciudad'            => 'persona_ciudad',
        'estado'            => 'persona_estado',
        'pais'              => 'persona_pais',
        'sexo'              => 'persona_sexo',
        'foto'              => 'persona_foto',
        'grupoSanguineo'    => 'persona_sanguineo',
        'alertas'           => 'persona_alertas',
        'esPaciente'        => 'persona_es_paciente',
        'esMedico'          => 'persona_es_medico',
        'expediente'        => 'persona_expediente',

        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}