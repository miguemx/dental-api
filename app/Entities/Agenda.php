<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Agenda extends Entity {
    protected $attributes = [
        'id'            => null,
        'medico'        => null,
        'paciente'      => null,
        'motivo'        => null,
        'fecha'         => null,
        'fechaFin'      => null,
        'status'        => null,
        'notas'         => null,
        'observaciones' => null,

        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    protected $datamap = [
        'id'            => 'agenda_id',
        'medico'        => 'agenda_medico',
        'paciente'      => 'agenda_paciente',
        'motivo'        => 'agenda_motivo',
        'fecha'         => 'agenda_fecha',
        'fechaFin'      => 'agenda_fecha_fin',
        'status'        => 'agenda_status',
        'notas'         => 'agenda_notas',
        'observaciones' => 'agenda_observaciones',

        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];
}