<?php
namespace App\Entities;

use CodeIgniter\Entity;

class PresupuestoItem extends Entity {

    protected $attributes = [
        'id'            => null,
        'presupuesto'   => null,
        'concepto'      => null,
        'unitario'      => null,
        'cantidad'      => null,
        'total'         => null,
        'descripcion'   => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    protected $datamap = [
        'id'            => 'presitem_id',
        'presupuesto'   => 'presitem_presupuesto',
        'concepto'      => 'presitem_concepto',
        'unitario'      => 'presitem_unitario',
        'cantidad'      => 'presitem_cantidad',
        'total'         => 'presitem_total',
        'descripcion'   => 'presitem_descripcion',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

}