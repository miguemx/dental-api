<?php
namespace App\Entities;
use CodeIgniter\Entity;

class Pago extends Entity {
    protected $attributes = [
        'id'            => null,
        'persona'       => null,
        'medico'        => null,
        'concepto'      => null,
        'subtotal'      => null,
        'impuestos'     => null,
        'total'         => null,
        'status'        => null,
        'formaPago'     => null,
        'fechaPago'     => null,
        'fechaPagar'    => null,

        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    protected $datamap = [
        'id'            => 'pago_id',
        'persona'       => 'pago_persona',
        'medico'       => 'pago_medico',
        'concepto'      => 'pago_concepto',
        'subtotal'      => 'pago_subtotal',
        'impuestos'     => 'pago_impuestos',
        'total'         => 'pago_total',
        'status'        => 'pago_status',
        'formaPago'     => 'pago_forma_pago',
        'fechaPago'     => 'pago_fecha_pago',
        'fechaPagar'    => 'pago_fecha_pagar',

        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];
}