<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\Pago;

class PagosModel extends Model {

    protected $table      = 'sgd_pagos';
    protected $primaryKey = 'pago_id';

    protected $returnType    = 'App\Entities\Pago';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'pago_persona', 'pago_medico', 'pago_concepto', 'pago_subtotal', 'pago_impuestos', 'pago_total',
        'pago_status', 'pago_forma_pago', 'pago_fecha_pago', 'pago_fecha_pagar'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    private $PersonaModel;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();

        $this->PersonaModel = new PersonaModel();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * crea un registro en la base de datos para dar de alta un pago de un paciente
     * @param data los datos en arreglo asociativo similares a la entidad Pago
     * @return result arreglo con las llaves de API para mostrar resultados
     */
    public function registra($data) {
        $result = [ 'status'=>'error', 'code'=>500, 'message'=>'Proceso de insercion no iniciado', 'data'=>null ];
        try {
            $pago = new Pago( $data );
            $pago->impuestos = $pago->impuestos * $pago->subtotal / 100;
            $pago->total = $pago->subtotal + $pago->impuestos; 
            $pago->fechaPago = date("Y-m-d");
            if ( $this->save($pago) ) {
                $pago->id = $this->lastId();
                $result = [ 'status'=>'ok', 'code'=>200, 'message'=>'Pago registrado correctamente.', 'data'=>$pago ];
            }
            else {
                $result = [ 'status'=>'error', 'code'=>500, 'message'=>'No se pudo crear el pago.', 'data'=>$pago ];
            }
        }
        catch ( \Exception $ex ) {
            var_dump( $ex );
            $result = [ 'status'=>'error', 'code'=>500, 'message'=>'Excepcion al insertar el pago.', 'data'=>$ex->getMessage() ];
        }
        return $result;
    }

    /**
     * obtiene los datos de los pagos realizados por el paciente especificado
     * @param paciente el ID del paciente
     * @param medico (opcional) el ID del medico
     * @return pagos la lista de pagos realizados
     * @return null En caso de que no existan u ocurra un error
     */
    public function paciente($paciente, $medico=null) {
        $this->where('pago_persona', $paciente);
        if ( !is_null($medico) ) {
            $this->where('pago_medico', $medico);
        }

        $pagos = $this->findAll();
        return $pagos;
    }

}