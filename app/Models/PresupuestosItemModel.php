<?php
namespace App\Models;

use CodeIgniter\Model;

class PresupuestosItemModel extends Model {
    
    protected $table      = 'sgd_presupuestos_items';
    protected $primaryKey = 'presitem_id';

    protected $returnType    = 'App\Entities\PresupuestoItem';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'presitem_presupuesto', 'presitem_concepto', 'presitem_unitario', 
        'presitem_cantidad', 'presitem_total', 'presitem_descripcion', 
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * agrega un item a un presupuesto
     * @param presupuestoItem los datos en un arreglo; el total se debe calcular dentro de este metodo; se incluye el ID del presupusto en la llave 'prersupuesto'
     * @return App\Entities\PresupuestoItem regresa el presupuesto creado
     * @return null cuando no se puede crear, se regresa un nulo
     */
    public function agrega($presupuestoItem) {
        $presupuestoItem->total = $presupuestoItem->unitario * $presupuestoItem->cantidad;
        if ( $this->insert($presupuestoItem) ) {
            $presupuestoItem->id = $this->lastId();
            return $presupuestoItem;
        }
        return null;
    }

    /**
     * obtiene todos los items de un presupuesto determinado
     * @param presupuestoId el ID del presupuesto para obtener los items
     */
    public function byPresupuesto($presupuestoId) {
        $this->where('presitem_presupuesto', $presupuestoId);
        return $this->findAll();
    }

}