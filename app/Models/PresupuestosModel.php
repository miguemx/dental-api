<?php
namespace App\Models;

use CodeIgniter\Model;

class PresupuestosModel extends Model {
    
    protected $table      = 'sgd_presupuestos';
    protected $primaryKey = 'presupuesto_id';

    protected $returnType    = 'App\Entities\Presupuesto';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'presupuesto_paciente', 'presupuesto_nombre', 'presupuesto_descripcion',
        'presupuesto_status', 'presupuesto_subtotal', 'presupuesto_impuesto', 'presupuesto_total',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();

    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene una lista de los presupuestos
     */
    public function lista($pagina=1, $regPorPagina=25, $filtros=array() ) {
        $this->creaFiltros($filtros);
        $this->orderBy('created_at');
        $presupuestos = $this->findAll();
        return $presupuestos;
    }

    /**
     * crea un filtro de busqueda para pacientes
     * @param filtros un array asociativo con los filtros, correspondientes en sus llaves a los campos de la entidad Persona
     */
    protected function creaFiltros($filtros) {
        if ( array_key_exists('paciente', $filtros ) ) {
            if ( is_numeric($filtros['paciente']) ) {
                $this->where( 'presupuesto_paciente', $filtros['paciente'] );
            }
        }

        if ( array_key_exists('status', $filtros ) ) {
            if ( strlen($filtros['status'])>0 ) {
                $this->where( 'presupuesto_status', $filtros['status'] );
            }
        }
    }

    /**
     * actualiza un registro de presupuesto en la base de datos
     * @param id el ID del registro  a modificar
     * @param data los datos en arreglo para actualizar 
     * @return result el resultado ( status=>ok|error code=>num message=>string data=>array con el presupuesto|null)
     */
    public function actualiza($id, $data) {
        $result = [ 'status'=>'error', 'code'=>500, 'message'=> 'No se puede crear el registro', 'data'=>null ];
        try {
            $presupuesto = $this->find( $id );
            $presupuesto->paciente      = ( array_key_exists('paciente',$data) )? $data['paciente']: $presupuesto->paciente;
            $presupuesto->nombre        = ( array_key_exists('nombre',$data) )? $data['nombre']: $presupuesto->nombre;
            $presupuesto->descripcion   = ( array_key_exists('descripcion',$data) )? $data['descripcion']: $presupuesto->descripcion;
            $presupuesto->status        = ( array_key_exists('status',$data) )? $data['status']: $presupuesto->status;
            // TODO actualizar los totales
            $this->update( $id, $presupuesto );
            $result = [ 'status'=>'ok', "code"=>"200", 'message'=> 'Presupuesto actualizado con éxito.', 'data'=>$presupuesto ];
        }
        catch (\Exception $ex) {
            $result = [ 'status'=>'error', "code"=>500, 
                'message'=> 'No se puede actualizar el presupuesto. Por favor verifique sus datos y que el presupuesto existe.', 
                'data'=>$ex->getMessage() 
            ];
        }                
        
       
        return $result;
    }

}