<?php

namespace App\Models;
use CodeIgniter\Model;

class PersonaModel extends Model {
    
    protected $table      = 'sgd_personas';
    protected $primaryKey = 'persona_id';

    protected $returnType    = 'App\Entities\Persona';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'persona_medico_cabecera', 'persona_nombre', 'persona_apellido', 'persona_correo', 'persona_celular',
        'persona_domicilio', 'persona_telefono', 'persona_documento_id', 'persona_curp', 'persona_edad', 'persona_fecha_nacimiento',
        'persona_ciudad', 'persona_estado', 'persona_pais', 'persona_sexo', 'persona_foto', 'persona_sanguineo',
        'persona_alertas', 'persona_es_paciente', 'persona_es_medico', 'persona_expediente'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca un paciente por el ID de la persona
     * @param id el ID de la persona a buscar
     * @return el objeto de entidad Persona, o null si no se encuenta
     */
    public function buscaPaciente($id) {
        $paciente = $this->where('persona_es_paciente','1')->find($id);
        return $paciente;
    }

    /**
     * crea un numero de expediente
     * @return numExpediente el numero del expediente generado
     */
    public function creaExpediente() {
        $identificador = rand( 100, 999 );
        $numExpediente = date("Ymd").time().$identificador;
        return $numExpediente;
    }

    /**
     * obtiene una lista de pacientes de acuerdo con los filtros proporcionados
     */
    public function getPacientes($pagina=1, $regPorPagina=25, $filtros=array() ) {
        $this->where('persona_es_paciente','1');
        $this->creaFiltros($filtros);
        $this->orderBy('persona_nombre');
        // var_dump( $this->builder->getCompiledSelect() );
        $pacientes = $this->findAll();
        return $pacientes;
    }

    /**
     * obtiene una lista de medicos de acuerdo con los filtros proporcionados
     */
    public function getMedicos($pagina=1, $regPorPagina=25, $filtros=array() ) {
        $this->where('persona_es_medico','1');
        $this->creaFiltros($filtros);
        $this->orderBy('persona_nombre');
        $pacientes = $this->findAll();
        return $pacientes;
    }

    /**
     * crea un filtro de busqueda para pacientes
     * @param filtros un array asociativo con los filtros, correspondientes en sus llaves a los campos de la entidad Persona
     */
    protected function creaFiltros($filtros) {
        if ( array_key_exists('nombre', $filtros ) ) {
            if ( strlen($filtros['nombre']) ) {
                $this->like( 'persona_nombre', $filtros['nombre'] );
            }
        }

        if ( array_key_exists('apellido', $filtros ) ) {
            if ( strlen($filtros['apellido']) ) {
                $this->like( 'persona_apellido', $filtros['apellido'] );
            }
        }

        if ( array_key_exists('medicoCabecera', $filtros ) ) {
            if ( is_numeric($filtros['medicoCabecera']) ) {
                $this->where( 'persona_medico_cabecera', $filtros['medicoCabecera'] );
            }
        }
    }

}