<?php
namespace App\Models;

use CodeIgniter\Model;
use App\Entities\Agenda as Agenda;

class AgendaModel extends Model {

    protected $table      = 'sgd_agenda';
    protected $primaryKey = 'agenda_id';

    protected $returnType    = 'App\Entities\Agenda';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'agenda_medico', 'agenda_paciente', 'agenda_motivo', 'agenda_fecha', 'agenda_fecha_fin',
        'agenda_status', 'agenda_notas', 'agenda_observaciones'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    private $PersonaModel;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();

        $this->PersonaModel = new PersonaModel();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * crea una entrada de agenda relacionando paciente y medico
     * @param data el arreglo con los datos a guardar en la agenda
     * @return result el resultado de la operacion de agregar la agenda
     */
    public function registra($data) {
        $result = [ 'status'=>'error', 'code'=>500, 'message'=> 'No se puede crear el registro', 'data'=>null ];
        $medico = $this->PersonaModel->find( $data['medico'] );
        $paciente = $this->PersonaModel->find( $data['paciente'] );
        if ( $medico && $paciente ) {
            if ( $medico->esMedico == '1' ) {
                $agenda = new Agenda( $data );
                if ( strtotime($agenda->fecha) < strtotime($agenda->fechaFin) ) {
                    $this->insert( $agenda );
                    $agenda->id = $this->lastId();
                    $result = [ 'status'=>'ok', "code"=>"200", 'message'=> 'Cita creada con éxito.', 'data'=>$agenda ];
                }
                else {
                    $result = [
                        'status'=>'error', 'code'=>'400', 
                        'message'=> 'La fecha/hora de finalizacion no es mayor a la fecha/hora de inicio.', 'data'=>null 
                    ];        
                }
            }
            else {
                $result = [
                    'status'=>'error', 'code'=>'400', 
                    'message'=> 'No se puede asignar una cita a una persona que no es medico.', 'data'=>null 
                ];    
            }
        }
        else {
            $result = [ 'status'=>'error', 'code'=>'404', 'message'=> 'No existe el paciente o el medico.', 'data'=>null ];
        }
        return $result;
    }

    /**
     * actualiza un registro de agenda
     * @param id el ID del registro de agenda a modificar
     * @param data los datos en arreglo para actualizar la agenda
     * @return result el resultado ( status=>ok|error code=>num message=>string data=>array con la agenda|null)
     */
    public function actualiza($id, $data) {
        $result = [ 'status'=>'error', 'code'=>500, 'message'=> 'No se puede crear el registro', 'data'=>null ];
        $agenda = $this->find( $id );
        if ( $agenda ) {
            $medico = $this->PersonaModel->find( $agenda->medico );
            if ( $medico->esMedico == '1' ) {
                $agenda->medico   = ( array_key_exists('medico',$data) )? $data['medico']: $agenda->medico;
                $agenda->paciente = ( array_key_exists('paciente',$data) )? $data['paciente']: $agenda->paciente;
                $agenda->motivo   = ( array_key_exists('motivo',$data) )? $data['motivo']: $agenda->motivo;
                $agenda->fecha    = ( array_key_exists('fecha',$data) )? $data['fecha']: $agenda->fecha;
                $agenda->fechaFin = ( array_key_exists('fechaFin',$data) )? $data['fechaFin']: $agenda->fechaFin;
                $agenda->status   = ( array_key_exists('status',$data) )? $data['status']: $agenda->status;
                $agenda->notas    = ( array_key_exists('notas',$data) )? $data['notas']: $agenda->notas;
                $agenda->observaciones = ( array_key_exists('observaciones',$data) )? $data['observaciones']: $agenda->observaciones;
                try {
                    if ( array_key_exists('fecha', $data) || array_key_exists('fechaFin', $data) ) {
                        if ( strtotime($data['fecha']) >= strtotime($data['fechaFin']) ) {
                            return [ 'status'=>'error', "code"=>"400", 'message'=> 'Parece que hay un error en la fecha de la cita.', 'data'=>null ];
                        }
                    }
                    $this->update( $id, $agenda );
                    $agenda->paciente = $this->PersonaModel->find( $agenda->paciente );
                    $result = [ 'status'=>'ok', "code"=>"200", 'message'=> 'Cita actualizada con éxito.', 'data'=>$agenda ];
                }
                catch (\Exception $ex) {
                    $result = [ 'status'=>'error', "code"=>"500", 
                        'message'=> 'Error al actualizar la cita. Puede deberse a que no existe el medico o paciente, o las fechas son incorrectas.', 'data'=>$ex->getMessage() ];
                }                
            }
            else {
                $result = [
                    'status'=>'error', 'code'=>'400', 
                    'message'=> 'No se puede asignar una cita a una persona que no es medico.', 'data'=>null 
                ];    
            }
        }
        else {
            $result = [ 'status'=>'error', 'code'=>'404', 'message'=> 'Verifique que el registro de la cita existe.', 'data'=>null ];
        }
        return $result;
    }

    /**
     * obtiene la agenda del medico en un periodo determinado por las fechas
     * @param idMedico el ID del medico a obtener la agenda
     * @param inicio la fecha de inicio en formato mysql
     * @param fin la fecha de fin en formato mysql
     * @return result un arreglo con los registros de la agenda
     */
    public function getMedico($idMedico, $inicio, $fin) {
        $rows = $this->where('agenda_medico',$idMedico)->where('agenda_fecha >=',$inicio)->where('agenda_fecha <=',$fin.' 23:59:59')->orderBy('agenda_fecha ASC')->findAll();
        if ( $rows ) {
            $personaModel = new PersonaModel();
            $temporal = array();
            foreach ( $rows as $row ) {
                $pacienteId = $row->paciente;
                $row->paciente = $personaModel->find( $pacienteId );
                if ( !is_null($row->paciente) ) {
                    $temporal[] = $row;
                }
            }
            $rows = $temporal;
        }
        return $rows;
    }

    /**
     * busca y retorna los registros en la base de datos de la agenda completa de un paciente
     * @param paciente el ID del paciente a buscar sus registros
     * @return agenda los registros de agenda de un paciente
     */
    public function getPaciente($paciente) {
        $this->where('agenda_paciente', $paciente);
        $this->orderBy('agenda_fecha DESC');
        return $this->findAll();
    }

}