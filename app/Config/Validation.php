<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//------------------------------------------------------------------------------------------------------
	// Rules
	//------------------------------------------------------------------------------------------------------

	// ------------------------------------ for URL parameters ---------------------------------------------
	public $urlparameters = [
        'id' => 'numeric',
	];
	public $urlparameters_errors = [
		'id' => [ 
			'numeric' => 'El parámetro de entrada no es correcto.'
		],
	];

	// -------------------- for signup ----------------------------------------------------------------------
	public $signup = [
        'username'        => 'required',
        'password'        => 'required',
		'passwordConfirm' => 'required|matches[password]',
		'name'         => 'required|regex_match[/^[A-Za-z\sñÑáéíóúÁÉÍÓÚ]+$/]',
		'email'           => 'required|valid_email',
		'cellphone'       => 'required|numeric'
	];
	public $signup_errors = [
		'username' => [ 'required' => 'You must choose an username.', ],
		'password' => [ 'required' => 'You must choose a password.', ],
		'passwordConfirm' => [ 
			'required' => 'Please confirm your password.',
			'matches' => 'Passwords do not match.',
		],
        'email' => [
			'required' => 'Please provide your email.',
            'valid_email' => 'Your email does not appear to be valid.'
		],
		'name' => [
			'required' => 'Please provide your name.',
            'regex_match' => 'Your name does not appear to be valid.'
        ]
		
	];
	
	// ------------------------------------ for login --------------------------------------------------
	public $login = [
        'username'        => 'required',
        'password'        => 'required',
	];
	public $login_errors = [
		'username' => [ 'required' => 'Por favor introduzca su nombre de usuario.', ],
		'password' => [ 'required' => 'Por favor introduzca su contraseña.', ],
	];

	// ------------------------------------ for register patients ----------------------------------------
	public $personas = [
        
        'nombre' => 'required|regex_match[/^[A-Za-z\sñÑáéíóúÁÉÍÓÚ]+$/]',
        'apellido' => 'required|regex_match[/^[A-Za-z\sñÑáéíóúÁÉÍÓÚ]+$/]',
		'celular' => 'required|numeric',
		
		// 'medicoCabecera' => 'numeric',		
        // 'telefono' => 'numeric',
        // 'curp' => 'regex_match[/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/]',
        // 'edad' => 'numeric',
        // 'fechaNacimiento' => 'regex_match[/^([0-2][0-9]|3[0-1])(\/|-)(0[1-9]|1[0-2])\2(\d{4})$/]',
        // 'ciudad' => 'regex_match[/^[A-Za-z\sñÑáéíóúÁÉÍÓÚ]+$/]',
        // 'estado' => 'regex_match[/^[A-Za-z\sñÑáéíóúÁÉÍÓÚ]+$/]',
        // 'pais' => 'regex_match[/^[A-Za-z\sñÑáéíóúÁÉÍÓÚ]+$/]',
        // 'sexo' => 'regex_match[/^([H]|[M]){1}$/i]',
        // 'foto' => 'regex_match[/\.(gif|jpe?g|png)$/i]',
        // 'grupoSanguineo' => 'regex_match[/^(A|B|AB|O)[+-]$/i]',
	];
	public $personas_errors = [
		
		'nombre' => [
			'required' => 'El campo Nombre es requerido',
			'regex_match' => 'Por favor ingrese un nombre válido'
		],
        'apellido' => [
			'required' => 'El campo Apellido es requerido',
			'regex_match' => 'Por favor ingrese un apellido válido'
		],
        
        'celular' => [
			'required' => 'El campo Celular es requerido',
			'numeric' => 'El Celular debe contener solo números'
		],

		// 'medicoCabecera' => ['numeric' => 'Por favor seleccione un médico de cabecera, o deje en blanco.'],
        // 'telefono' => ['numeric' => 'El Telefono debe contener solo números'],
        // 'curp' => ['regex_match' => 'El CURP tiene un formato incorrecto'],
        // 'edad' => ['numeric' => 'La edad debe ser numérica'],
        // 'fechaNacimiento' => ['regex_match' => 'Verifique la fecha de nacimiento'],
        // 'ciudad' => ['regex_match' => 'El nombre de la Ciudad parece ser incorrecto'],
        // 'estado' => ['regex_match' => 'El nombre de la Ciudad parece ser incorrecto'],
        // 'pais' => ['regex_match' => 'El nombre de la Ciudad parece ser incorrecto'],
        // 'sexo' => ['regex_match' => 'El dato del género debe de ser solamente H para Hombres o M para Mujeres'],
        // 'foto' => ['regex_match' => 'El nombre del archivo de imagen parece ser incorrecto'],
        // 'grupoSanguineo' => ['regex_match' => 'El grupo sanguíneo parece ser incorrecto'],
	];

	// ------------------------------------ for listados --------------------------------------------------
	public $listados = [
        'pagina'        => 'required|numeric',
        'registros' 	=> 'required|numeric',
	];
	public $listados_errors = [
		'pagina' 	=> [ 'required' => 'Se necesita la pagina.', 'numeric' => 'La pagina debe ser numerica.' ],
		'registros' => [ 'required' => 'Se necesita el numero de registros.', 'numeric' => 'El numero de registros es incorrecto.' ],
	];


	// ------------------------------------ for crear agenda --------------------------------------------------
	public $agenda = [
        'medico'    => 'required|numeric',
		'paciente' 	=> 'required|numeric',
		'motivo' 	=> 'required',
		'fecha' 	=> 'required|regex_match[/^\d\d\d\d\-([0]{1}[1-9]|1[012])\-([0][1-9]|([012][0-9])|(3[01]))\s([0-1]?[0-9]|2?[0-3]):([0-5]\d):([0-5]\d)$/]',
		'fechaFin' 	=> 'required|regex_match[/^\d\d\d\d\-([0]{1}[1-9]|1[012])\-([0][1-9]|([012][0-9])|(3[01]))\s([0-1]?[0-9]|2?[0-3]):([0-5]\d):([0-5]\d)$/]',
		'status' 	=> 'required|in_list[EN ESPERA,ATENDIDA,CANCELADA,FALTA]',
		
	];
	public $agenda_errors = [
		'medico' 	=> [ 'required' => 'Por favor especifique el médico.', 'numeric' => 'El médico no se puede encontrar.' ],
		'paciente' => [ 'required' => 'Por favor especifique el paciente.', 'numeric' => 'El paciente no se puede encontrar.' ],
		'motivo' => [ 'required' => 'El motivo de la cita es requerido.' ],
		'fecha' => [ 'required' => 'Es necesario indicar la fecha y hora de la cita.', 'regex_match' => 'La fecha y hora de inicio no tiene un formato correcto.' ],
		'fechaFin' => [ 'required' => 'Es necesario indicar la fecha de término.', 'regex_match' => 'La fecha y hora de salida no tiene un formato correcto.' ],
		'status' => [ 'required' => 'Estado de la cita es necesario.', 'in_list' => 'Estado de la cita no es reconocido.' ],
	];


	// ------------------------------------ for presupuestos --------------------------------------------------
	public $presupuestos = [
        'paciente'    	=> 'required|numeric',
		'nombre' 		=> 'required|alpha_numeric_punct',
		'descripcion' 	=> 'if_exist|alpha_numeric_punct',
		'status' 		=> 'required|in_list[SOLICITADO,APROBADO]',
		'impuesto'		=> 'if_exist|integer'
	];
	public $presupuestos_errors = [
		'paciente' 		=> [ 'required' => 'Por favor especifique el paciente.', 'numeric' => 'El paciente no se puede encontrar.' ],
		'nombre' 		=> [ 'required' => 'Por favor proporcione un título de presupuesto.', 'alpha_numeric_punct'=>'El título del presupuesto no parece ser correcto' ],
		'descripcion' 	=> [ 'if_exist' => 'No tiene una descripcion correcta.', 'alpha_numeric_punct' => 'La descripcion no parece ser correcta.' ],
		'status' 		=> [ 'required' => 'Es necesario indicar el status del presupuesto.', 'in_list' => 'Status de presupuesto no reconocido.' ],
		'impuesto'		=> [ 'integer' =>'El porcentaje de impuesto debe ser entero', 'if_exist'=>'Si envia el impuesto, debe ser valor entero.' ]
	];

	// ------------------------------------ for pagos de pacientes --------------------------------------------------
	public $pagos = [
		'paciente'    	=> 'required|numeric',
		'medico'    	=> 'required|numeric',
		'concepto' 		=> 'required',
		'subtotal' 	    => 'required|decimal',
		'impuestos'		=> 'required|integer',
		'status' 		=> 'in_list[PENDIENTE,PAGADO]',
		'formaPago'		=> 'in_list[EFECTIVO,TARJETA,CHEQUE,TRANSFERENCIA,OTRO]',
		'fechaPago'		=> 'if_exist|regex_match[/^\d\d\d\d\-([0]{1}[1-9]|1[012])\-([0][1-9]|([012][0-9])|(3[01]))\s([0-1]?[0-9]|2?[0-3]):([0-5]\d):([0-5]\d)$/]',
		'fechaPagar'	=> 'if_exist|regex_match[/^\d\d\d\d\-([0]{1}[1-9]|1[012])\-([0][1-9]|([012][0-9])|(3[01]))\s([0-1]?[0-9]|2?[0-3]):([0-5]\d):([0-5]\d)$/]'
		
	];
	public $pagos_errors = [
		'paciente'    	=> ['required'=>'Por favor especifique un paciente.', 'numeric'=>'El dato del paciente es erróneo.'],
		'medico'    	=> ['required'=>'Por favor especifique al médico que le está pagando.', 'numeric'=>'El dato del médico es erróneo.'],
		'concepto' 		=> ['required'=>'Por favor especifique un concepto.'],
		'subtotal' 	    => ['required'=>'Por favor especifique el subtotal.', 'decimal'=>'El subtotal parece ser incorrecto.'],
		'impuestos'		=> ['required'=>'Por favor especifique el porcentaje de impuestos.', 'integer'=>'El impuesto debe ser en porcentaje entero.'],
		'status' 		=> ['in_list'=>'Debe proporcionar un estado válido.'],
		'formaPago'		=> ['in_list'=>'Debe seleccionar una forma de pago válida.'],
		'fechaPago'		=> ['regex_match'=>'La fecha de pago parece ser incorrecta.'],
		'fechaPagar'	=> ['regex_match'=>'La fecha de venciminto parece ser incorrecta.']
	];

}
