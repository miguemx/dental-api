<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data = [];
		$this->response->setStatusCode(403)->setBody("You don't have permission.");
		$this->response->setJson($data);
	}

}
