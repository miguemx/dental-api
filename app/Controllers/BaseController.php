<?php
namespace App\Controllers;

use Firebase\JWT\JWT;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];
	protected $Validacion;

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
		$this->Validacion = \Config\Services::validation();
	}

	/**
	 * pasa al fomtato json la respuesta para eliminar los campos inecesarios
	 * @param data el arreglo final con el cual se hara la respuesta
	 * @return response la respuesta en formato json para ser renderizada por el controlador
	 */
	protected function cleanResponse($data) {
		$response = json_encode ( $data ); // convertir en string la respuesta para evitar trabajar con objetos
		$response = json_decode ( $response, true ); // regresar a array asociativo la respuesta
		$response = $this->cleanData( $response ); // eliminar las llaves innecesarias y se sigue manteniendo el array
		$response = json_encode( $response ); // volver a generar el string json para ser renderizado
		return $response;
	}

	/**
	 * elimina los campos inecesarios de la data que sera enviada como respuesta
	 * @param data los datos en un array asociativo 100%
	 * @return data los datos ya limpios
	 */
	private function cleanData( $data ) {
		foreach ( $data as $key=>$value ) {
			if ( gettype($data[$key]) == 'array' ) {
				$data[$key] = $this->cleanData( $data[$key] );
			}
			if ( strpos( $key, '_') !== false ) {
				unset( $data[$key] );
			}
		}
		return $data;
	}

	/**
     * obtiene los datos de un arreglo que tenga las llaves con un prefijo para identificar su tipo de informacion
     * @param data el arreglo donde vienen los datos que se desea obtener
     * @param tipo una cadena con el prefijo
     * @return prefixedData el arreglo con los datos obtenidos; las llaves regresan de forma camelCase
     */
    protected function getPrefixedData($data, $tipo) {
        $prefixedData = array();
        foreach ( $data as $key=>$value ) {
            if ( preg_match("/^".$tipo."/", $key) ) {
                $key = str_replace( $tipo, '', $key );
                $key = lcfirst( $key );
                $prefixedData[$key] = $value;
            }
        }
        
        return ( $prefixedData );
	}
	
	/**
     * regresa los errores detectados por la libreria Validation en un arreglo con los textos
	 * @param validation le instancia de la libreria validation
	 * @return errors un arreglo con los mensajes de error
     */
    protected function getValidationErrors($validation) {
        $errors = array();
        foreach ( $validation->getErrors() as $error ) {
            $errors[] = $error;
        }
        return $errors;
	}

	/**
	 * realiza la validacion de un token de sesion
	 * @param jwt el token a validar
	 * @return result arreglo con el resultado de la validacion [ 'status'=>'ok|error', 'message'=>'resultado de validacion', 'data'=>datos]
	 */
	protected function validaToken($jwt) {
		$result = ['status'=>'error', 'message'=>'No se ha iniciado la validacion', 'data'=>null];
		try {
			$key = env('jwt.secretkey');
			$data = JWT::decode($jwt, $key, array('HS256'));
			$result = ['status'=>'ok', 'message'=>'Valid token', 'data'=>$data];
		}
		catch ( \Firebase\JWT\SignatureInvalidException $ex) {
			$result = ['status'=>'error', 'message'=>$ex->getMessage(), 'data'=>'token sign'];
		}
		catch ( \Firebase\JWT\ExpiredException $ex) {
			$result = ['status'=>'error', 'message'=>"Sesión expirada", 'data'=>'token expired'];
		}
		catch ( \DomainException $ex) {
			$result = ['status'=>'error', 'message'=>$ex->getMessage(), 'data'=>'token domain'];
		}
		catch ( \Exception $ex) {
			$result = ['status'=>'error', 'message'=>'Generic exception. '.$ex->getMessage(), 'data'=>'token'];
		}
		return $result;
	}

	/**
	 * crea un token de sesion
	 * @param datos los datos del usuario que ha iniciado sesion
	 * @return token el token generado;
	 */
	protected function creaToken($datos) {
		$time = time();
		$key = env('jwt.secretkey');

		$token = array(
			'iat' => $time, // Tiempo que inició el token
			'exp' => $time + (60*60*15), // Tiempo que expirará el token (+15 min)
			'data' => $datos, // informacion del usuario
		);

		$jwt = JWT::encode($token, $key);

		return $jwt;
	}

	/**
	 * valida si la sesion del cliente es valida o no
	 * @param header la cabecera donde reside el token de sesion
	 * @return token si la sesion es correcta, regenera un token y lo regresa; devuelve null si la sesion no es correcta
	 */
	protected function validaSesion($header) {
		$token = null;
		if ( !is_null($header) ) {
			$result = $this->validaToken( $header->getValue() );
			if ( $result['status'] == 'ok' ) {
				$token = $this->creaToken( $result['data']->data );
			}
		}
		return $token;
	}

	/**
	 * valida los permisos de acceso para los roles de los usuarios del sistema
	 * @param header el header donde reside el token de sesion
	 * @return result array con la bandera de exito o fallo, el nuevo token y los datos de permisos
 	 */
	protected function validaAcceso($header) {
		$result = [ 'status'=>'error', 'code'=>'401', 'message'=>'401. Sesión expirada.', 'data'=>'acceso' ];
		$token = $this->validaSesion($header);
		if ( $token ) {
			// TODO validar los permisos del rol aqui
			$result = [ 'status'=>'ok', 'message'=>'Granted.', 'token'=>$token, 'data'=>null ];
		}
		return $result;
	}

	/**
	 * invoca la libreria de validacion y regresa el resultado de dicha validacion
	 * @param data los datos a validar en un array asociativo
	 * @param regla una cadena con la regla de validacion a utilizar, registrada en la libreria de validacion
	 * @return true verdadero si no existe ningun error
	 * @return result arreglo [ status=>ok|error, message=>mensaje recibido, data=> si hay errores, el arreglo con el listado ]
	 */
	protected function valida($data, $regla) {
		if ( $this->Validacion->run( $data, $regla ) ) { 
			return true;
		}
		else {
			$errors = array();
			foreach ( $this->Validacion->getErrors() as $error ) {
				$errors[] = $error;
			}
			$result = ['status'=>'error', 'code'=>'400', 'message'=>'Algunos valores proporcionados no son correctos.', 'data'=>$errors];
			return $result;
		}
	}

}
