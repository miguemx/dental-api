<?php
namespace App\Controllers;

use App\Models\PresupuestosModel;
use App\Models\PresupuestosItemModel;
use App\Entities\Presupuesto;
use App\Entities\PresupuestoItem;
use App\Models\PersonaModel;

use CodeIgniter\I18n\Time;

class Presupuestos extends BaseController {

    private $PresupuestosModel;
    private $PresupuestosItemModel;
    private $PersonaModel;

    public function __construct() {
        $this->PresupuestosModel = new PresupuestosModel();
        $this->PresupuestosItemModel = new PresupuestosItemModel();
        $this->PersonaModel = new PersonaModel();
    }

    /**
     * listado de pacientes, que recibe por post los filtros
     */
    public function index() {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            $response = $this->valida($data, 'listados');
            if ( $response === true ) { // validar la integridad de los campos
                $filtros = (array_key_exists('filtros',$data) && is_array($data['filtros']) )? $data['filtros']: array();
                $presupuestos = $this->PresupuestosModel->lista( $data['pagina'], $data['registros'], $filtros );
                foreach ( $presupuestos as $i => $presupuesto ) {
                    $paciente = $this->PersonaModel->withDeleted()->find( $presupuesto->paciente );
                    if ( $paciente ) {
                        $presupuestos[$i]->paciente = $paciente;
                        $presupuestos[$i]->fecha = $presupuesto->created_at->toDateString();
                    }
                }
                $response = ['status'=>'ok', 'code'=>'200', 'message'=>'Presupuestos listados correctamente.', 'data'=> $presupuestos ];
            }
        }
        
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * creacion de presupuestos
     */
    public function crea() {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            $response = $this->valida($data, 'presupuestos');
            if ( $response === true ) { // validar la integridad de los campos
                // $pacientes = $this->PresupuestosModel->lista( $data['pagina'], $data['registros'], $filtros );
                $presupuesto = new Presupuesto( $data );
                $presupuesto->subtotal = $presupuesto->total = 0;
                if ( $this->PresupuestosModel->save( $presupuesto ) ) {
                    $items = array();
                    $presupuesto->id = $this->PresupuestosModel->lastId();
                    foreach ( $data['items'] as $item ) {
                        $presupuestoItem = new PresupuestoItem( $item );
                        $presupuestoItem->presupuesto = $presupuesto->id;
                        if ( !$this->PresupuestosItemModel->agrega( $presupuestoItem ) ) {
                            $response = ['status'=>'error', 'code'=>'550', 'message'=>'Uno de los Items no puede ser registrado.', 'data'=> $item ];
                            $this->PresupuestosModel->delete( $presupuesto->id );
                            break;        
                        }
                        $presupuesto->subtotal += $presupuestoItem->total;
                        $items[] = $presupuestoItem;
                    }
                    $presupuesto->impuesto = $presupuesto->impuesto / 100 * $presupuesto->subtotal;
                    $presupuesto->total = $presupuesto->subtotal + $presupuesto->impuesto;
                    $presupuesto->items = $items;
                    $this->PresupuestosModel->update( $presupuesto->id, $presupuesto );
                    $response = ['status'=>'ok', 'code'=>'200', 'message'=>'Presupuesto creado correctamente.', 'data'=> $presupuesto];
                }
                else {
                    $response = ['status'=>'error', 'code'=>'550', 'message'=>'No se puede guardar el presupuesto.', 'data'=> null];
                }
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * muestra la informacion de un presupuesto determinado por su ID
     * @param id el ID del presupuesto a obtener
     */
    public function ver($id) {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            if ( is_numeric($id) ) { // validar la integridad de los campos
                $presupuesto = $this->PresupuestosModel->find( $id );
                if ( $presupuesto ) {
                    $items = $this->PresupuestosItemModel->byPresupuesto($presupuesto->id);
                    $presupuesto->items = $items;
                    $response = ['status'=>'ok', 'code'=>200, 'message'=>'Presupuesto obtenido correctamente.', 'data'=>$presupuesto ];
                }
                else {
                    $response = ['status'=>'error', 'code'=>404, 'message'=>'Presupuesto no encontrado.', 'data'=>null ];    
                }
            }
            else {
                $response = ['status'=>'error', 'code'=>400, 'message'=>'Parámetro incorrecto.', 'data'=>null ];
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * muestra la informacion de un presupuesto determinado por su ID
     * @param id el ID del presupuesto a obtener
     */
    public function editar($id) {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            // obtener los datos actuales del presupuesto para completar las llaves obligatorias
            $presupuesto = $this->PresupuestosModel->find( $id );
            $data['paciente'] = $presupuesto->paciente;
            if ( !array_key_exists( 'nombre', $data) ) $data['nombre'] = $presupuesto->nombre;
            // ahora si se sigue con el flujo normal
            $response = $this->valida($data, 'presupuestos');
            if ( $response === true ) {
                $response = $this->PresupuestosModel->actualiza($presupuesto->id, $data);
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}