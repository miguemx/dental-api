<?php
namespace App\Controllers;

use App\Models\AgendaModel as AgendaModel;
use App\Models\PersonaModel as PersonaModel;

class Agenda extends BaseController {

    private $AgendaModel;
    private $PersonaModel;

    public function __construct() {
        $this->AgendaModel = new AgendaModel();
        $this->PersonaModel = new PersonaModel();
    }

    public function index() {
        $response = ['status'=>'error', 'message'=>'401. Unauthorized', 'data'=>null];
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * busca en la base de datos la agenda del medico
     * @param id el ID de la tabla personas donde esta persona debe ser un medico
     */
    public function medico($id) {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            if ( $data ) {
                $registros = $this->AgendaModel->getMedico( $id, $data['inicio'], $data['fin'] );
                $response = [ 'status'=>'ok', 'message'=>'Registros listados correctamente', 'data'=> $registros];
            }
            else {
                $response = [ 'status'=>'error', 'message'=>'Parámetros incorrectos.', 'data'=> null ];
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * regresa los datos de todos los registros de agenda de un paciente especifico
     * @param paciente el ID del paciente
     */
    public function paciente($paciente) {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            if ( is_numeric($paciente) ) {
                $registros = $this->AgendaModel->getPaciente( $paciente );
                $response = [ 'status'=>'ok', 'message'=>'Registros listados correctamente', 'data'=> $registros];
            }
            else {
                $response = [ 'status'=>'error', 'code'=>400, 'message'=>'Parámetros incorrectos.', 'data'=> null ];
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * devuelve el json de una cita (agenda) mediante su ID
     * @param id el ID de la agenda
     */
    public function ver($id) {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            if ( is_numeric($id) ) {
                $agenda = $this->AgendaModel->find( $id );
                if ( $agenda ) {
                    $agenda->medico = $this->PersonaModel->find( $agenda->medico );
                    $agenda->paciente = $this->PersonaModel->find( $agenda->paciente );;
                    $response = [ 'status'=>'ok', 'code'=>200, 'message'=>'Cita listada correctamente.', 'data'=>$agenda ];
                }
                else {
                    $response = [ 'status'=>'error', 'code'=>404, 'message'=>'No se encontró la cita.', 'data'=>null ];
                }
            }
            else {
                $response = [ 'status'=>'error', 'code'=>400, 'message'=>'No se recibió el parámetro para localizar la cita.', 'data'=>null ];
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * entry point para registrar datos en la agenda
     */
    public function registra() {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            $response = $this->valida($data, 'agenda');
            if ( $response === true ) {
                $response = $this->AgendaModel->registra( $data );
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }
    
    /**
     * modifica la informacion de un registro de agenda
     */
    public function edita($id) {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            if ( !is_null($data) ) {
                $response = $this->AgendaModel->actualiza( $id, $data );
            }
            else {
                $response = [ 'status'=>'error', 'code'=>400, 'message'=>'Faltan parámetros para realizar esta acción.', 'data'=>null];
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}