<?php
namespace App\Controllers;

use App\Models\PersonaModel as PersonaModel;

class Medicos extends BaseController {

    protected $PersonaModel;

    public function __construct() {
        $this->PersonaModel = new PersonaModel();
    }

    public function index() {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok' ) {
            $token = $response['token'];
            $data = $this->request->getJSON(true); 
            if ( $this->Validacion->run( $data, 'listados' ) ) { // validar la integridad de los campos
                $filtros = (array_key_exists('filtros',$data) && is_array($data['filtros']) )? $data['filtros']: array();
                $pacientes = $this->PersonaModel->getMedicos( $data['pagina'], $data['registros'], $filtros );
                $response = ['status'=>'ok', 'message'=>'Pacientes listados correctamente.', 'data'=> ['registros'=>$pacientes] ];
                $this->response->setHeader('xapiauth', $token);
            }
            else {
                foreach ( $this->Validacion->getErrors() as $error ) {
                    $errors[] = $error;
                }
                $response = ['status'=>'error', 'message'=>'Validación de datos.', 'data'=>$errors];
            }
        }
        
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}