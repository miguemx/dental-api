<?php
namespace App\Controllers;

use App\Models\PersonaModel as PersonaModel;
use App\Entities\Persona as Persona;

class Pacientes extends BaseController {

    protected $Validation;

    protected $PersonaModel;
    protected $Persona;

    public function __construct() {
        $this->PersonaModel = new PersonaModel();
        $this->Persona = new Persona();
        $this->Validation = \Config\Services::validation();
    }

    /**
     * controla el funcionamiento por defecto
     */
    public function index() {
        $response = ['status'=>'error', 'message'=>'Proceso no iniciado.', 'data'=>null];
        if( $token = $this->validaSesion( $this->request->getHeader('xapiauth') ) ) {
            $data = $this->request->getJSON(true); 
            if ( $this->Validation->run( $data, 'listados' ) ) { // validar la integridad de los campos
                $filtros = (array_key_exists('filtros',$data) && is_array($data['filtros']) )? $data['filtros']: array();
                $pacientes = $this->PersonaModel->getPacientes( $data['pagina'], $data['registros'], $filtros );
                $response = ['status'=>'ok', 'message'=>'Pacientes listados correctamente.', 'data'=> ['registros'=>$pacientes] ];
                $this->response->setHeader('xapiauth', $token);
            }
            else {
                foreach ( $this->Validation->getErrors() as $error ) {
                    $errors[] = $error;
                }
                $response = ['status'=>'error', 'message'=>'Validación de datos.', 'data'=>$errors];
            }
        }
        else {
            $response = ['status'=>'error', 'message'=>'401. Sesion expirada.', 'data'=>null];
        }
        
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * entry point para crear pacientes
     */
    public function crea() {
        $response = ['status'=>'error', 'message'=>'Proceso no iniciado.', 'data'=>null];
        if( $token = $this->validaSesion( $this->request->getHeader('xapiauth') ) ) {
            $data = $this->request->getJSON(true); 
            if ( $this->Validation->run( $data, 'personas' ) ) { // validar la integridad de los campos
                $this->Persona = new Persona( $data );
                $this->Persona->esPaciente = '1';
                $this->Persona->expediente = $this->PersonaModel->creaExpediente();
                if ( $this->PersonaModel->save( $this->Persona ) ) {
                    $this->Persona->id = $this->PersonaModel->lastId();
                    $response = ['status'=>'ok', 'message'=>'Paciente registrado.', 'data'=> $this->Persona];
                }
                else {
                    $response = ['status'=>'error', 'message'=>'No se puede crear el registro del paciente.', 'data'=>null];
                }
            }
            else {
                foreach ( $this->Validation->getErrors() as $error ) {
                    $errors[] = $error;
                }
                $response = ['status'=>'error', 'message'=>'Validación de datos.', 'data'=>$errors];
            }
        }
        else {
            $response = ['status'=>'error', 'message'=>'401. Sesion expirada.', 'data'=>null];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * busca el ID del paciente en la base de datos
     * @param id el Id del paciente (persona) a buscar
     */
    public function ver($id) {
        $response = ['status'=>'error', 'message'=>'Proceso no iniciado.', 'data'=>null];
        if( $token = $this->validaSesion( $this->request->getHeader('xapiauth') ) ) {
            $persona = $this->PersonaModel->buscaPaciente( $id );
            if ( !is_null($persona) ) { 
                $response = ['status'=>'ok', 'message'=>'Paciente encontrado.', 'data'=> $persona ];
                $this->response->setHeader('xapiauth', $token);
            }
            else {
                $response = ['status'=>'error', 'message'=>'Paciente no existe.', 'data'=>null];
            }
        }
        else {
            $response = ['status'=>'error', 'message'=>'401. Sesion expirada.', 'data'=>null];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * @deprecated 
     */
    public function editar() {
        $response = ['status'=>'error', 'message'=>'Proceso no iniciado.', 'data'=>null];
        if( $token = $this->validaSesion( $this->request->getHeader('xapiauth') ) ) {
            $data = $this->request->getJSON(true); 
            if ( $this->Validation->run( $data, 'personas' ) ) { // validar la integridad de los campos
                if ( array_key_exists('id', $data) ) {
                    $this->Persona = $this->PersonaModel->find( $data["id"] );
                    if ( $this->Persona ) {
                        if ( $this->Persona->esPaciente == '1'  ) {
                            $this->Persona = new Persona( $data );
                            if ( $this->PersonaModel->save( $this->Persona ) ) {
                                $response = ['status'=>'ok', 'message'=>'Se ha guardado con éxito.', 'data'=>$this->Persona];            
                            }
                            else {
                                $response = ['status'=>'error', 'message'=>'No se pudo guardar la información.', 'data'=>null];    
                            }
                        }
                        else {
                            $response = ['status'=>'error', 'message'=>'No cuenta con permisos para editar esta persona.', 'data'=>null];
                        }
                    }
                    else {
                        $response = ['status'=>'error', 'message'=>'No se ha encontrado el registro de la persona.', 'data'=>null];
                    }
                    
                }
                else {
                    $response = ['status'=>'error', 'message'=>'Falta un parámetro.', 'data'=>null];        
                }
            }
            else {
                foreach ( $this->Validation->getErrors() as $error ) {
                    $errors[] = $error;
                }
                $response = ['status'=>'error', 'message'=>'Validación de datos.', 'data'=>$errors];
            }
        }
        else {
            $response = ['status'=>'error', 'message'=>'401. Sesion expirada.', 'data'=>null];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * entry point para la eliminacion de pacientes
     */
    public function eliminar() {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            $response = $this->valida($data, 'urlparameters');
            if ( $response === true ) { 
                if ( $this->PersonaModel->delete( $data['id'] ) ) {
                    $response = [ 'status'=>'ok', 'code'=>200, 'message'=>'Paciente eliminado correctamente.', 'data'=>null ];
                }
                else {
                    $response = [ 'status'=>'error', 'code'=>500, 'message'=>'No se pudo eliminar al paciente.', 'data'=>null ];
                }
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}