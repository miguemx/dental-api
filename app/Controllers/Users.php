<?php

namespace App\Controllers;

use App\Models\UsuarioModel as UsuarioModel;
use App\Models\PersonaModel as PersonaModel;

use \App\Libraries\Mail as Mail;

class Users extends BaseController {

    protected $UsuarioModel;
    protected $PersonaModel;

    protected $Validation;

    public function __construct() {
        $this->Validation =  \Config\Services::validation();
        $this->UsuarioModel = new UsuarioModel();
        $this->PersonaModel = new PersonaModel();
    }

    /**
     * list all users
     */
    public function index() {
        $this->response->setHeader('Content-Type', 'application/json');
        $response = array(
            'status' => 'ok',
            'message' => 'Data retrived',
            'data' => 'Users'
        );
        echo json_encode( $response );
    }

    /**
     * verifica los datos del usuario para permitirle iniciar sesion
     */
    public function login() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( $this->Validation->run( $data, 'login' ) ) { // validar la integridad de los campos
            $usuario = $this->UsuarioModel->login( $data['username'], $data['password'] );
            if ( !is_null($usuario) ) {
                $userData = ['pantalla'=>'pacientes', 'id'=>$usuario->id, 'un'=>$usuario->username, 'rol'=>$usuario->rol, 'pr' => $usuario->persona];
                $token = $this->creaToken( $userData );
                // $userData['token'] = $token;
                $response = [ 'status'=>'ok', 'message'=>'Inicio de sesión correcto.', 'data'=>$userData ];
                $this->response->setHeader( 'xapiauth', $token );
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Usuario y/o contraseña no coinciden.', 'data' => null );    
            }         
        }
        else {
            foreach ( $this->Validation->getErrors() as $error ) {
                $errors[] = $error;
            }
            $response = array( 'status' => 'error', 'message' => 'No se pudo iniciar sesión.', 'data' => $errors );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * crea un usuario desde la app
     */
    public function crea() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( $this->validation->run( $data, 'signup' ) ) { // validar la integridad de los campos
            
        }
        else {
            foreach ( $this->validation->getErrors() as $error ) {
                $errors[] = $error;
            }
            $response = array( 'status' => 'error', 'message' => 'Cannot create new user.', 'data' => $errors );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * obtiene la información de un usuario con base en el ID proporcionado
     */
    public function ver($id) {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
        $data = [ 'id'=>$id ];
        if ( $this->validation->run( $data, 'urlparameters') ) {
            $perfil = $this->users->getPerfil( $id );
            if ( $perfil ) {
                $response = array( 'status' => 'ok', 'message' => 'User\'s profile obtained.', 'data' => $perfil );
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'User not found.', 'data' => $perfil );
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Validation errors.', 'data' => $this->getValidationErrors($this->validation) );
        }

        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * verifica un toke de activacion
     */
    public function verify($token) {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
        $UserModel = new UserModel();

        $validationResult = $UserModel->validaToken( $token );
        if ( $validationResult === true ) {
            $response = array( 'status' => 'ok', 'message' => 'User has been validated.', 'data' => null );
        }
        else {
            $response = array( 'status' => 'error', 'message' => $validationResult, 'data' => null );
        }

        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * 
     */
    public function resend($userId) {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => $this->user );
        if ( is_numeric($userId) ) {
            $user = $this->users->find( $userId );
            if ( !is_null($user) ) {
                $persona = $this->personas->find( $user->person );
                if ( !is_null($persona) ) {
                    $user->active = '0';
                    $user->validationToken = $this->users->creaToken( $user->username );
                    $this->users->save( $user );
                    $mail = new Mail();
                    $mail->registration( $persona->email, $persona->name, $user->validationToken );
                    $response = array( 'status' => 'ok', 'message' => 'Token sent.', 'data' => $user );
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Cannot find person data.', 'data' => null );
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Username cannot be found.', 'data' => null );
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Cannot find user with information provided.', 'data' => null );
        }
        
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    public function checkToken() {
        $response = ['status'=>'error', 'message'=>'Proceso no iniciado.','data'=>null];
        $data = $this->request->getJSON(true); 
        if ( !is_null($data) ) {
            $response = $this->validaToken( $data['token'] );
            if ( $response['status'] == 'ok') {
                $this->response->setHeader('xapiauth', $data['token']);
            }
        }
        else {
            $response = ['status'=>'error', 'message'=>'Parámetros incorrectos.','data'=>null];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}