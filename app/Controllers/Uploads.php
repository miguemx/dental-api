<?php
namespace App\Controllers;

class Uploads extends BaseController {

    public function __construct() {

    }

    public function fotos() {
        $response = [ 'status'=>'error', 'message'=>'Proceso no iniciado', 'data'=>null ];

        $file = $this->request->getFile('userfile');
        if ( !is_null($file) ) {
            if ($file->isValid() && ! $file->hasMoved()) {
                $newName = 'P'.time().rand(1000,9999).'.'.$file->getClientExtension();
                $file->move( env('app.publicdir').'pphotos', $newName );
                $response = [ 'status'=>'ok', 'message'=>'Proceso no iniciado', 'data'=>$newName ];
            }
            else {
                $response = [ 'status'=>'error', 'message'=>'Ocurrió un error al momento de subir el archivo', 'data'=>$file->getErrorString() ];    
            }
        }
        else {
            $response = [ 'status'=>'error', 'message'=>'Por favor suba un archivo válido', 'data'=>null ];
        }
        
        
        $this->response->setHeader("Content-type", "application/json");
        echo $this->cleanResponse( $response );
    }

}