<?php
namespace App\Controllers;

use App\Models\PagosModel;
use App\Models\PersonaModel;
use App\Models\PresupuestosModel;

class Pagos extends BaseController {

    // TODO Ejecutar la siguiente consulta
    // ALTER TABLE `sgd_pagos` ADD `pago_medico` INT UNSIGNED NOT NULL AFTER `pago_persona`;
    // ALTER TABLE sgd_pagos ADD FOREIGN KEY fkpagosmedicos(pago_medico) REFERENCES sgd_personas(persona_id) ON UPDATE CASCADE on DELETE CASCADE


    protected $PagosModel;
    protected $PersonaModel;
    protected $PresupuestosModel;

    public function __construct() {
        $this->PagosModel = new PagosModel();
        $this->PersonaModel = new PersonaModel();
        $this->PresupuestosModel = new PresupuestosModel();
    }

    /**
     * muestra un listado de los pagos que se han registrado;
     * en los filtros se puede especificar al medico que le corresponde
     */
    public function index() {
        
    }

    /**
     * registra un pago en la cuenta de un paciente; recibe por post los datos del pago
     * 
     */
    public function registra() {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            $data = $this->request->getJSON(true);
            $response = $this->valida($data, 'pagos');
            if ( $response === true ) {
                if ( $this->PersonaModel->find($data['paciente']) && $this->PersonaModel->find($data['medico']) ) {
                    $data['persona'] = $data['paciente'];
                    $response = $this->PagosModel->registra( $data );
                }
                else {
                    $response = [ 'status'=>'error', 'code'=>404, 'message'=>'Paciente o médico no encontrados', 'data'=>null ];
                }
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * devuelve un concentrado de los pagos realizados contra el saldo pendiente
     * de un paciente
     * @param paciente el ID del paciente para obtener su estado de cuenta
     */
    public function saldo($paciente) {
        $response = $this->validaAcceso( $this->request->getHeader('xapiauth') );
        if( $response['status'] == 'ok'  ) {
            $this->response->setHeader('xapiauth', $response['token']);
            if ( is_numeric($paciente) ) {
                $paciente = $this->PersonaModel->find( $paciente );
                if ( $paciente ) {
                    $pagos = $this->PagosModel->paciente( $paciente->id );
                    $presupuestos = $this->PresupuestosModel->lista(1,99999, ['paciente'=>$paciente->id ]);
                    $debe = $pagado = 0;
                    foreach ( $presupuestos as $presupuesto ) {
                        if ( $presupuesto->status === 'APROBADO') 
                            $debe += $presupuesto->total;
                    }
                    foreach ( $pagos as $pago ) $pagado += $pago->total;
                    $saldo = $debe - $pagado;
                    $data = [ 'saldo'=>$saldo, 'deuda'=>$debe, 'pagado'=>$pagado, 'detalle'=>[ 'presupuestos'=>$presupuestos, 'pagos'=>$pagos ] ];
                    $response = [ 'status'=>'ok', 'code'=>200, 'message'=>'Saldo consultado correctamente.', 'data'=> $data ];
                }
                else {
                    $response = [ 'status'=>'error', 'code'=>404, 'message'=>'Paciente no encontrado.', 'data'=>null ];
                }
            }
            else {
                $response = [ 'status'=>'error', 'code'=>400, 'message'=>'Parámetro no válido.', 'data'=>null ];
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}